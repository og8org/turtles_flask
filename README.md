# README

Small flashcard application to study languages.
Both sides of a card have to be added manually and can be studied after.

Flask and uwsgi is used in backend, frontend is a simple template with ajax to query for cards.
Nginx can be used to run as a webserver with socket connection to uwsgi.

## Screenshots
![](/screenshots/covered.png?raw=true)
![](/screenshots/uncovered.png?raw=true)


