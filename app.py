from flask import Flask, render_template, request, redirect, url_for
app = Flask(__name__)

import json
import datetime
import random
from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError

@app.route("/turtles/")
def index():
    return render_template('index.html',index='turtles')

@app.route("/turtles/verbs")
def verbs():
    return render_template('index.html',index='verbs')

@app.route("/turtles/memrise")
def memrise():
    return render_template('index.html',index='memrise')

@app.route("/turtles/add")
def add():
    return render_template('add.html')

@app.route("/turtles/words/", methods=['GET'])
def words():
    index = request.values.get('index','turtles')
    es = Elasticsearch()
    res = []
    query = json.dumps({
      "query": {
          "function_score" : {
            "query" : { "match_all": {} },
            "random_score" : {}
          }
        }
    })
    try:
        res = es.search(index=index,body=query,size=30)['hits']['hits']
    except NotFoundError:
        es.indices.create("turtles")
        res = []
    random.shuffle(res)
    wordll = [ (x['_source']['bt1'],x['_source']['bt2']) for x in res ]
    esids = [ x['_id'] for x in res ]
    return json.dumps({'words':wordll,'esids':esids})

@app.route("/turtles/addme", methods=['POST'])
def addme():
    es = Elasticsearch()
    x = request.form
    bt1 = x.get('bt1','')
    bt2 = x.get('bt2','')
    body = json.dumps({'bt1':bt1,'bt2':bt2,'read': 0,'created': datetime.datetime.now().strftime('%Y-%m-%dT%H:%M')})
    es.index('turtles',body=body)
    return json.dumps('ACK')

@app.route("/turtles/deleteme", methods=["POST"])
def delme():
    es = Elasticsearch()
    x = request.form
    esid = x.get('id')
    if esid:
       try:
           es.delete('turtles',id=esid)
       except Exception as e:
           return json.dumps('Error')
    return json.dumps('ACK')

@app.route("/turtles/rateme", methods=["POST"])
def rateme():
    es = Elasticsearch()
    x = request.form
    esid = x.get('id')
    try:
        diff = int(x.get('diff'))
    except ValueError:
        return json.dumps('Error')
    if esid and isinstance(diff,int):
       try:
           es.update('turtles',id=esid, body={'doc':{'diff':diff}})
       except Exception as e:
           return json.dumps('Error')
    return json.dumps('ACK')

if __name__ == "__main__":
    app.run()
